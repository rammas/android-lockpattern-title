# `7.0.0` _(October 28th, 2015)_

Dependencies:

- `com.android.support:support-annotations:+`

Changes:

- Made dependency `support-annotations` always latest version.
- Removed `haibison.android.lockpattern.utils.`~~`LoadingDialog`~~.
